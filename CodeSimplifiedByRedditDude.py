N = 10
text = ''

for x in range(0, N+1):
    for y in range(0, N-x+1):
        text += "{{c1::" + str(x) +"}} + {{c2::" + str(y) + "}} = {{c3::" + str(x+y) + "}};\n"

    for y in range(0, x+1):
        text += "{{c1::" + str(x) +"}} - {{c2::" + str(y) + "}} = {{c3::" + str(x-y) + "}};\n"

with open('export.txt', 'w') as f:
    f.write(text)